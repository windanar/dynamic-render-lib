# dynamic-render-lib

> dynamic render

[![NPM](https://img.shields.io/npm/v/dynamic-render-lib.svg)](https://www.npmjs.com/package/dynamic-render-lib) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save dynamic-render-lib
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'dynamic-render-lib'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [Win Rar](https://github.com/Win Rar)
